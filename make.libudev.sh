################################################################################
sep_start;echo 'libudev:compile src files'
for libudev_src_file in $libudev_src_files
do
	libudev_o_file_name=$(basename $libudev_src_file .c)
	libudev_o_file=$(dirname $libudev_src_file)/${libudev_o_file_name}.o

	echo "LIBUDEV_CC $libudev_src_file-->$libudev_o_file"

	$libudev_cc	-o $libudev_o_file \
			-D_GNU_SOURCE \
			$INTERNAL_CPPFLAGS \
			-I. \
			-I$src_path \
			$src_path/src/$libudev_src_file

	libudev_o_files="$libudev_o_file $libudev_o_files"
done
sep_end

#-------------------------------------------------------------------------------

sep_start;echo 'libudev:link the object files to produce the dynamically linked shared library '
echo "LIBUDEV_CCLD libudev.so.${libudev_api}.0.0"
mkdir -p -- $fake_root$e_libdir

$libudev_ccld	-o $fake_root$e_libdir/libudev.so.${libudev_api}.0.0 \
		-Wl,--version-script=$src_path/src/libudev.sym \
		$libudev_o_files

ln -s libudev.so.${libudev_api}.0.0 $fake_root$e_libdir/libudev.so.${libudev_api}
ln -s libudev.so.${libudev_api} $fake_root$e_libdir/libudev.so

sep_end
################################################################################
sep_start;echo 'libudev:fake installing headers'
mkdir -p -- $fake_root$e_includedir
cp -f $src_path/src/libudev.h $fake_root$e_includedir
sep_end
