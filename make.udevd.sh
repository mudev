udevd_all_src_files="
$udevd_common_src_files
$udevd_src_files
$udevadm_src_files
"
################################################################################
sep_start;echo 'udevd:compile src files'
for udevd_all_src_file in $udevd_all_src_files
do
	# build a file name which is prefixed with 'd', 'D'ynamic
	udevd_all_o_file_name=$(basename $udevd_all_src_file .c)
	udevd_all_o_file=$(dirname $udevd_all_src_file)/${udevd_all_o_file_name}.o

	echo "BIN_CC $udevd_all_src_file-->$udevd_all_o_file"

	$bin_cc		-o $udevd_all_o_file \
			$INTERNAL_CPPFLAGS \
			-D_GNU_SOURCE \
			-I. \
			-I$src_path \
			$src_path/src/$udevd_all_src_file

	udevd_all_o_files="$udevd_all_o_file $udevd_all_o_files"
done
sep_end

#-------------------------------------------------------------------------------

for udevd_common_src_file in $udevd_common_src_files
do
	udevd_common_o_file_name=$(basename $udevd_common_src_file .c)
	udevd_common_o_file=$(dirname $udevd_common_src_file)/${udevd_common_o_file_name}.o
	udevd_common_o_files="$udevd_common_o_file $udevd_common_o_files"
done

for udevd_src_file in $udevd_src_files
do
	udevd_o_file_name=$(basename $udevd_src_file .c)
	udevd_o_file=$(dirname $udevd_src_file)/${udevd_o_file_name}.o
	udevd_o_files="$udevd_o_file $udevd_o_files"
done

for udevd_libudev_src_file in $udevd_libudev_src_files
do
	udevd_libudev_o_file_name=$(basename $udevd_libudev_src_file .c)
	udevd_libudev_o_file=$(dirname $udevd_libudev_src_file)/${udevd_libudev_o_file_name}.o
	udevd_libudev_o_files="$udevd_libudev_o_file $udevd_libudev_o_files"
done

for udevadm_src_file in $udevadm_src_files
do
	udevadm_o_file_name=$(basename $udevadm_src_file .c)
	udevadm_o_file=$(dirname $udevadm_src_file)/${udevadm_o_file_name}.o
	udevadm_o_files="$udevadm_o_file $udevadm_o_files"
done

for udevadm_libudev_src_file in $udevadm_libudev_src_files
do
	udevadm_libudev_o_file_name=$(basename $udevadm_libudev_src_file .c)
	udevadm_libudev_o_file=$(dirname $udevadm_libudev_src_file)/${udevadm_libudev_o_file_name}.o
	udevadm_libudev_o_files="$udevadm_libudev_o_file $udevadm_libudev_o_files"
done
#-------------------------------------------------------------------------------

mkdir -p -- $fake_root$e_eprefix/bin

#-------------------------------------------------------------------------------

sep_start;echo 'dudevd:link the object files to produce the dynamic binary'
echo "BIN_CCLD udevd"
$bin_ccld	-o $fake_root$e_eprefix/bin/udevd \
		$udevd_o_files \
		$udevd_common_o_files \
		$udevd_libudev_o_files \
		-L$fake_root$e_libdir -ludev
sep_end

#-------------------------------------------------------------------------------

sep_start;echo 'udevadm:link the object files to produce the dynamic binary'
echo "BIN_CCLD udevadm"

$bin_ccld	-o $fake_root$e_eprefix/bin/udevadm \
		$udevadm_o_files \
		$udevd_common_o_files \
		$udevadm_libudev_o_files \
		-L$fake_root$e_libdir -ludev
sep_end
################################################################################
