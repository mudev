#!/bin/sh

#this script is brutal and verbose, has no tricks and is quite linear, then
#quite easy to deal with
#for the moment, it's hardcoded for a gcc toolchain... BAD! Since now
#gcc is a c++ toxic tantrum 

# stolen from ffmpeg configure like a pig

# Prevent locale nonsense from breaking basic text processing.
LC_ALL=C
export LC_ALL

libudev_api=0
fake_root=fake_root

#-------------------------------------------------------------------------------
libudev_src_files='
libudev.c
libudev-device.c
libudev-device-private.c
libudev-enumerate.c
libudev-list.c
libudev-monitor.c
libudev-queue.c
libudev-queue-private.c
libudev-util.c
libudev-util-private.c
'
udevd_common_src_files='
udev-event.c
udev-watch.c
udev-node.c
udev-rules.c
udev-ctrl.c
udev-builtin.c
udev-builtin-blkid.c
udev-builtin-hwdb.c
udev-builtin-input_id.c
udev-builtin-kmod.c
udev-builtin-path_id.c
udev-builtin-usb_id.c
'
udevd_libudev_src_files='
libudev.c
libudev-device-private.c
libudev-device.c
libudev-list.c
libudev-monitor.c
libudev-queue-private.c
libudev-queue.c
libudev-util-private.c
libudev-util.c
'
udevd_src_files='
udevd.c
'
udevadm_libudev_src_files='
libudev.c
libudev-device-private.c
libudev-device.c
libudev-list.c
libudev-monitor.c
libudev-util-private.c
libudev-util.c
'
udevadm_src_files='
udevadm.c
udevadm-info.c
udevadm-control.c
udevadm-monitor.c
udevadm-settle.c
udevadm-trigger.c
udevadm-test.c
udevadm-test-builtin.c
'

#-------------------------------------------------------------------------------

sep_start()
{
printf '###############################################################################\n'
}

sep_end()
{
printf '###############################################################################\n\n'
}

subsep_start()
{
printf '*******************************************************************************\n'
}

subsep_end()
{
printf '*******************************************************************************\n'
}

################################################################################

is_in()
{
value=$1
shift
for var in $*; do
	[ $var = $value ] && return 0
done
return 1
}

die_unknown()
{
echo "Unknown option \"$1\"."
echo "See $0 --help for available options."
exit 1
}

set_default()
{
for opt; do
	eval : \${$opt:=\$${opt}_default}
done
}

spaces_concat()
{
printf "$1" | tr -s '[:space:]' ' '
}

CMDLINE_SET='
bin_cc
bin_ccld
libudev_cc
libudev_ccld
prefix
eprefix
libdir
includedir
sysconfdir
pkglibexecdir
usb_database
pci_database
version
'

################################################################################

#command line set defaults
#===============================================================================
#This defaults are for gcc, tested with version 4.7.4. You will need to
#override those for you compiler (tinycc/open64/pcc...). Additionnally, source
#support for different toolchains is not done.
#-------------------------------------------------------------------------------
# dynamically linked binaries
bin_cc_default="gcc		-Wall -Wextra -Wno-unused-parameter \
				-Wno-missing-field-initializers \
				-static-libgcc \
				-std=c99 -O2 -c"

bin_ccld_default="gcc		-Wl,--as-needed"
#-------------------------------------------------------------------------------
# 'D'ynamically linked 'S'hared library
libudev_cc_default="gcc	-Wall -Wextra -Wno-unused-parameter \
				-Wno-implicit-fallthrough \
				-static-libgcc \
				-std=c99 -O2 -fPIC -c"

#************MUST ADD A VERSION SCRIPT*******************
libudev_ccld_default="gcc	-shared \
				-Wl,-soname,libudev.so.$libudev_api \
				-Wl,--as-needed"
#===============================================================================

prefix_default=/usr/local
eprefix_default='$prefix'
libdir_default='$eprefix/lib'
includedir_default='$prefix/include'
sysconfdir_default='$prefix/etc'
pkglibexecdir_default='$libdir/udev'
usb_database_default='$prefix/share/hwdata/usb.ids'
pci_database_default='$prefix/share/hwdata/pci.ids'
version_default=189
set_default $CMDLINE_SET

################################################################################

show_help(){
    cat <<EOF
Usage: make [options]

Options: [defaults in brackets after descriptions]

Help options:
  --help                               print this message

Standard options:
  --enable-logging                     enable logging code paths
  --enable-debug                       enable debug code paths

  --prefix=PREFIX                      architecture independent prefix [$prefix_default]
  --eprefix=EPREFIX                    architecture dependent exec prefix [$eprefix_default]
  --libdir=DIR                         object code libraries [$libdir_default]
  --includedir=DIR                     C header files [$includedir_default]
  --sysconfdir=DIR                     read-only single-machine data [$sysconfdir_default]
  --pkglibexecdir=DIR                  program executables [$pkglibexecdir_default]

  --usb-database=FILE_PATH             the usb database path [$usb_database_default]
  --pci-databased=FILE_PATH            the pci database path [$pci_database_default]

  --version=VERSION                    override the version number [$version_default]

  --man                                build man pages

Advanced options:
  --bin-cc=CC                          use C compiler command line CC for target dynamic binaries [$(spaces_concat "$bin_cc_default")]
  --bin-ccld=CCLD                      use linker command line CCLD for target dynamic binaries [$(spaces_concat "$bin_ccld_default")]

  --libudev-cc=CC                      use C compiler command line CC for target dynamically linked shared libudev [$(spaces_concat "$libudev_cc_default")]
  --libudev-ccld=CCLD                  use linker command line CCLD for target dynamically linked shared libudev [$(spaces_concat "$libudev_ccld_default")]
EOF
  exit 0
}

################################################################################

for opt do
	optval="${opt#*=}"
	case "$opt" in
		--help|-h) show_help
		;;
		--enable-logging)
			INTERNAL_CPPFLAGS="$INTERNAL_CPPFLAGS -DENABLE_LOGGING"
		;;
		--enable-debug)
			INTERNAL_CPPFLAGS="$INTERNAL_CPPFLAGS -DENABLE_DEBUG"
		;;
		--man)
			MAN=yes
		;;
		*)
			optname=${opt%%=*}
			optname=${optname#--}
			optname=$(echo "$optname" | sed 's/-/_/g')
			if is_in $optname $CMDLINE_SET; then
				eval $optname='$optval'
			else
				die_unknown $opt
			fi
		;;
	esac
done

################################################################################

path_expand()
{
	e_v=$1
	#we set a maximum expansion depth of 3
	for d in 1 2 3
	do
		e_v=$(eval echo "$e_v")
	done
	#get rid of ugly double // in path
	echo "$e_v" | sed -e 's%//%/%g'
}

sep_start;echo 'expanding final paths:'
e_prefix=$(path_expand "$prefix")
e_eprefix=$(path_expand "$eprefix")
e_libdir=$(path_expand "$libdir")
e_includedir=$(path_expand "$includedir")
e_sysconfdir=$(path_expand "$sysconfdir")
e_pkglibexecdir=$(path_expand "$pkglibexecdir")
e_usb_database=$(path_expand "$usb_database")
e_pci_database=$(path_expand "$pci_database")
echo "prefix=$e_prefix"
echo "eprefix=$e_eprefix"
echo "libdir=$e_libdir"
echo "includedir=$e_includedir"
echo "sysconfdir=$e_sysconfdir"
echo "pkglibexecdir=$e_pkglibexecdir"
echo "usb_database=$e_usb_database"
echo "pci_database=$e_pci_database"
sep_end

################################################################################

sep_start;echo 'looking for source path:'
src_path=$(readlink -f $(dirname "$0"))
echo "source path is $src_path";sep_end

################################################################################

INTERNAL_CPPFLAGS="$INTERNAL_CPPFLAGS -DSYSCONFDIR=\"$e_sysconfdir\""
INTERNAL_CPPFLAGS="$INTERNAL_CPPFLAGS -DPKGLIBEXECDIR=\"$e_pkglibexecdir\""
INTERNAL_CPPFLAGS="$INTERNAL_CPPFLAGS -DUSB_DATABASE=\"$e_usb_database\""
INTERNAL_CPPFLAGS="$INTERNAL_CPPFLAGS -DPCI_DATABASE=\"$e_pci_database\""
INTERNAL_CPPFLAGS="$INTERNAL_CPPFLAGS -DVERSION=\"$version\""

################################################################################

. $src_path/make.libudev.sh
. $src_path/make.udevd.sh

################################################################################

mkdir -p -- "$fake_root/$e_libdir/pkgconfig"

sep_start;echo 'generating pkg-config file for udev'
cp -f $src_path/src/udev.pc.in "$fake_root/$e_libdir/pkgconfig/udev.pc"
sed -i "s%@VERSION@%$version%" "$fake_root/$e_libdir/pkgconfig/udev.pc"
sed -i "s%@pkglibexecdir@%$e_pkglibexecdir%" "$fake_root/$e_libdir/pkgconfig/udev.pc"
sep_end

sep_start;echo 'generating pkg-config file for libudev'
cp -f $src_path/src/libudev.pc.in "$fake_root/$e_libdir/pkgconfig/libudev.pc"
sed -i "s%@VERSION@%$version%" "$fake_root/$e_libdir/pkgconfig/libudev.pc"
sed -i "s%@prefix@%$e_prefix%" "$fake_root/$e_libdir/pkgconfig/libudev.pc"
sed -i "s%@exec_prefix@%$e_eprefix%" "$fake_root/$e_libdir/pkgconfig/libudev.pc"
sed -i "s%@libdir@%$e_libdir%" "$fake_root/$e_libdir/pkgconfig/libudev.pc"
sed -i "s%@includedir@%$e_includedir%" "$fake_root/$e_libdir/pkgconfig/libudev.pc"
sep_end

################################################################################

sep_start;echo 'fake installing the system udev rules'
mkdir -p -- "$fake_root/$e_pkglibexecdir"
rm -Rf -- "$fake_root/$e_pkglibexecdir/rules.d"
cp -rf -- "$src_path/rules" "$fake_root/$e_pkglibexecdir/rules.d"
sep_end

################################################################################

if [ -z "$MAN" ]; then
	exit 0
fi
#hardly tested...
sep_start;echo 'generating man pages'
mkdir -p -- "$fake_root/$e_prefix/share/man8" "$fake_root/$e_prefix/share/man7"
xsltproc -o "$fake_root/$e_prefix/share/man7/udev.7" -nonet http://docbook.sourceforge.net/release/xsl/current/manpages/docbook.xsl "$src_path/src/udev.xml"
xsltproc -o "$fake_root/$e_prefix/share/man8/udevadm.8" -nonet http://docbook.sourceforge.net/release/xsl/current/manpages/docbook.xsl "$src_path/src/udevadm.xml"
xsltproc -o "$fake_root/$e_prefix/share/man8/udevd.8" -nonet http://docbook.sourceforge.net/release/xsl/current/manpages/docbook.xsl "$src_path/src/udev.xml"
sep_end
