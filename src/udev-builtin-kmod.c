/*
 * load kernel modules
 *
 * Copyright (C) 2020 Sylvain BERTRAND <sylvain.bertrand@legeek.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include "udev.h"
#define loop for(;;)
static void busybox_modprobe_exec(struct udev *udev, char *alias) 
{
	loop {
		errno = 0;
		info(udev, "modprobe:exec for %s\n", alias);
		execlp("modprobe", "modprobe", alias, 0);
		if (errno != EAGAIN) {
			err(udev, "modprobe:unable to exec\n");
			exit(1);
		}
		/* EAGAIN */
	}
	/* unreachable */
}
/* we fork here */
static int load_module(struct udev *udev, char *alias)
{
	pid_t modprobe_pid;
	int r;
	int modprobe_status;

	modprobe_pid = fork();
	if (modprobe_pid == -1) {
		err(udev, "modprobe:unable to fork in order to exec\n");
		return EXIT_FAILURE;
	} else if (modprobe_pid == 0) {
		busybox_modprobe_exec(udev, alias);
		/* unreachable */
	} 
	r = waitpid(modprobe_pid, &modprobe_status, 0);
	if (r == -1) {
		err(udev, "modprobe:unable to wait for on pid %d\n", modprobe_pid);
		return EXIT_FAILURE;
	}
	if (WIFEXITED(modprobe_status)) {
		if (WEXITSTATUS(modprobe_status) != 0) {
			err(udev, "mobprobe:exit status %d\n", WEXITSTATUS(modprobe_status));
			return EXIT_FAILURE;
		}
		/* WEXITSTATUS(modprobe_status) == 0 */
		return EXIT_SUCCESS;
	} 
	err(udev, "modprobe:exited abnormally\n");
	return EXIT_FAILURE;
}
static int builtin_kmod(struct udev_device *dev, int argc, char *argv[], bool test)
{
        struct udev *udev = udev_device_get_udev(dev);
        int i;

        if (argc < 3 || strcmp(argv[1], "load")) {
                err(udev, "modprobe:expect: %s load <module>\n", argv[0]);
                return EXIT_FAILURE;
        }
        for (i = 2; argv[i]; i++) {
                info(udev, "modprobe:execute '%s' '%s'\n", argv[1], argv[i]);
                load_module(udev, argv[i]);
        }
        return EXIT_SUCCESS;
}
/* called at udev startup */
static int builtin_kmod_init(struct udev *udev)
{
        info(udev, "initing busybox modprobe:empty\n");
        return 0;
}
/* called on udev shutdown and reload request */
static void builtin_kmod_exit(struct udev *udev)
{
        info(udev, "exiting busybox modprobe:empty\n");
}
/* called every couple of seconds during event activity; 'true' if config has changed */
static bool builtin_kmod_validate(struct udev *udev)
{
        info(udev, "validation of busybox module databases done at invokation time\n");
        return true;
}
const struct udev_builtin udev_builtin_kmod = {
        .name = "kmod",
        .cmd = builtin_kmod,
        .init = builtin_kmod_init,
        .exit = builtin_kmod_exit,
        .validate = builtin_kmod_validate,
        .help = "kernel module loader",
        .run_once = false,
};
